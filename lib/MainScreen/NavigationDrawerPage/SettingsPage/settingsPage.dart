import 'package:flutter/material.dart';

import '../../../main.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        backgroundColor: header,
        title: Center(
          child: Container(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Settings",
                      style: TextStyle(fontSize: 18, color: Colors.white)),
                ],
              ),
            ),
          ),
        ),
        // actions: <Widget>[
        //   Container(
        //       margin: EdgeInsets.only(right: 15), child: Icon(Icons.search))
        // ],
      ),
      //////////////////////////  Scaffold BOdy Start ///////////////////////////////
      body: Container(
        color: Colors.white,
        //alignment: Alignment.centerLeft,
        child: Container(
          decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
          child: Container(
            padding: EdgeInsets.only(top: 12, bottom: 0, left: 20, right: 20),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  ///////// item List Start ///////////

                  /////////////   Profile Start ////////////
                  GestureDetector(
                      onTap: () {},
                      child: itemList(
                        Icon(
                          Icons.person,
                          color: Color(0XFFFFFFFF),
                        ),
                        'Profile',
                        Color(0XFF707070).withOpacity(0.3),
                      )),
                  /////////////   Profile End ////////////

                  ////////////////   General Start ////////////
                  GestureDetector(
                      onTap: () {},
                      child: itemList(
                        Icon(
                          Icons.settings,
                          color: Color(0XFFFFFFFF),
                        ),
                        'General',
                        Color(0XFF707070).withOpacity(0.3),
                      )),
                  /////////////   General End ///////////////

                  /////////////   Switch Account Start ////////////
                  GestureDetector(
                    onTap: () {},
                    child: itemList(
                      Icon(Icons.settings_backup_restore,
                          color: Color(0XFFFFFFFF)),
                      'Switch Account',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                  ),
                  /////////////   Switch Account End ////////////

                  /////////////   Terms And Conditions Start ////////////
                  GestureDetector(
                    onTap: () {},
                    child: itemList(
                      Icon(
                        Icons.note,
                        color: Color(0XFFFFFFFF),
                      ),
                      'Terms And Conditions',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                  ),
                  /////////////   Terms And Conditions End ////////////

                  /////////////   Privacy Start ////////////
                  GestureDetector(
                    onTap: () {},
                    child: itemList(
                      Icon(
                        Icons.lock,
                        color: Color(0XFFFFFFFF),
                      ),
                      'Privacy',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                  ),
                  /////////////   Privacy End ////////////

                  /////////////   Help Start ////////////
                  GestureDetector(
                    onTap: () {},
                    child: itemList(
                      Icon(
                        Icons.help,
                        color: Color(0XFFFFFFFF),
                      ),
                      'Help',
                      Color(0XFF707070).withOpacity(0.3),
                    ),
                  ),
                  /////////////   Help End ////////////

                  /////////////   About Start ////////////
                  GestureDetector(
                    onTap: () {},
                    child: itemList(
                      Icon(
                        Icons.people,
                        color: Color(0XFFFFFFFF),
                      ),
                      'About',
                      Colors.white,
                      // Color(0XFF707070).withOpacity(0.3),
                    ),
                  ),
                  /////////////   About End ////////////
                ],
                //children: _mainList(),
              ),
            ),
          ),
        ),
      ),

      ///
      ///
    );
  }

  Column itemList(Icon icons, String title, Color color) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(5.0, 8.0, 5.0, 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(right: 10.0),
                        decoration: BoxDecoration(
                          color: header,
                          // color: Color(0XFF596F97),
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        ),
                        child: icons,
                      ),
                      Container(
                        child: Text(
                          title,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black45,
                // color: Color(0XFF596F97),
                size: 13,
              )
            ],
          ),
        ),
        Divider(color: color)
      ],
    );
  }
}
